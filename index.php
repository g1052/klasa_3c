<?php
$server = "localhost";
$user = "root";
$pass = "";
$database = "datab";

$conn = new mysqli($server, $user, $pass, $database);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}


$sql = "SELECT id, question_text, answer1, answer2, answer3, answer4 FROM survey_questions";
$result = $conn->query($sql);
?>

<form method="post" action="submit.php">
<?php
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {

    echo "<p>" . $row["question_text"]. "</p>";
    echo "<input type='radio' name='q1_" . $row["id"]. "' value='" . $row["answer1"] . "'>" . $row["answer1"];
    echo "<input type='radio' name='q2_" . $row["id"]. "' value='" . $row["answer2"] . "'>" . $row["answer2"];
    echo "<input type='radio' name='q3_" . $row["id"]. "' value='" . $row["answer3"] . "'>" . $row["answer3"];
    echo "<input type='radio' name='q4_" . $row["id"]. "' value='" . $row["answer4"] . "'>" . $row["answer4"];

  }
} else {
  echo "0 results";
}
$conn->close();
?>

<input type="submit" value="Submit">
</form>
