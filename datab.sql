CREATE DATABASE IF NOT EXISTS `datab` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `datab`;


CREATE TABLE IF NOT EXISTS `survey_questions` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
  `question_text` text NOT NULL,
	`answer1` text NOT NULL,
	`answer2` text NOT NULL,
	`answer3` text NOT NULL,
	`answer4` text NOT NULL,

	PRIMARY KEY (`id`)
);

INSERT INTO `survey_questions` (`question_text`, `answer1`, `answer2`, `answer3`, `answer4`)
VALUES
('What is your favorite movie?', 'Action', 'Comedy', 'Drama', 'Science Fiction'),
('What is your favorite sport?', 'Soccer', 'Basketball', 'Tennis', 'Golf'),
('What is your favorite season?', 'Spring', 'Summer', 'Fall', 'Winter'),
('What is your favorite book genre?', 'Mystery', 'Romance', 'Science Fiction', 'Fantasy'),
('What is your favorite hobby?', 'Painting', 'Cooking', 'Gardening', 'Reading');

CREATE TABLE IF NOT EXISTS `survey_answers` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,

	`question_id` int(11) NOT NULL,
	`answer` text NOT NULL,
	PRIMARY KEY (`id`)

);

